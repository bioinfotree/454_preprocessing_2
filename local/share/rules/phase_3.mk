### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

SPECIES ?=

ADDRESSES ?=
README ?=
EVAL_BLAST ?=
EVAL_FILTER ?=
QUERY_COV ?=


extern ../phase_1/$(IDX1).fasta as INPUT_FASTA
extern ../phase_1/$(IDX1).fasta.qual as INPUT_QUAL



# produce variables that link blast database belonging to phase_2 for each specie in $(SPECIES) with extern
# generate a makefile that is subsequently evaluated
# the variable name produced is: speciename_ADDRESS := ftp://ftp.ensemblgenomes.org/...
addresses.mk:
	>$@; \
	array1=($(SPECIES)); \
	count=$${#array1[@]}; \
	for i in `seq 1 $$count`; \
	do \
	echo -e "extern ../phase_2/$${array1[$$i-1]} as $${array1[$$i-1]}_LINK" >>$@; \
	done


include addresses.mk



# links
query.fasta: $(INPUT_FASTA)
	ln -sf $< $@

query.qual: $(INPUT_QUAL)
	ln -sf $< $@

# use extern file asseciated to corresponding variable
%.flag:
	ln -sf $($*_LINK) $(basename $@); \
	touch $@

%.xml.gz: query.fasta %.flag
	!threads
	$(call search_DB, $<, ./$(basename $^2)/$(basename $^2), $@)

%.tab: %.xml.gz
	$(call parse_result, $<, $@)





define search_DB
	blastn -evalue $(EVAL_BLAST) -num_threads $$THREADNUM -outfmt 5 -max_target_seqs 20 -query $1 -db $2 -out $(basename $3); \
	if gzip -cv $(basename $3) > $3; \
	then \
	rm $(basename $3); \
	fi
endef


define parse_result
	zcat -dc $1 | blast_fields -f \
	-r application,database_length,database,database_sequences,expect,query,query_length \
	-a accession,hit_def,hit_id,length \
	-p align_length,bits,expect,frame,gaps,identities,num_alignments,positives,query,query_end,query_start,sbjct,sbjct_end,sbjct_start,score,strand \
	-e $(EVAL_FILTER) \
	-q $(QUERY_COV) \
	>$2
endef



.PHONY: test
test:
	@echo $(ADDRESSES)



ALL   += $(addsuffix .flag, $(SPECIES)) \
	 $(addsuffix .xml.gz, $(SPECIES)) \
	 $(addsuffix .tab, $(SPECIES))

INTERMEDIATE += 

CLEAN += 


################################################
### phase_1.mk ends here
