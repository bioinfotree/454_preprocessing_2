### phase_1.mk --- 
## 
## Filename: phase_1.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Wed Aug  3 15:24:25 2011 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

SPECIES ?=

ADDRESSES ?=
README ?=
EVAL_BLAST ?=
EVAL_FILTER ?=
QUERY_COV ?=
ALN_IDENTITY ?=


# extern ../phase_1/$(IDX1).fasta as INPUT_FASTA
# extern ../phase_1/$(IDX1).fasta.qual as INPUT_QUAL
extern ../phase_1/$(IDX1)_454_cleaned_newbler.fastq as INPUT_FASTQ






# associate each name defined in $(SPECIES) with corresponding ftp address in $(ADDRESSES)
# generate a makefile that is subsequently evaluated
# the variable name produced is: speciename_ADDRESS := ftp://ftp.ensemblgenomes.org/...
addresses.mk:
	>$@; \
	array1=($(SPECIES)); \
	array2=($(ADDRESSES)); \
	array3=($(README)); \
	count=$${#array1[@]}; \
	for i in `seq 1 $$count`; \
	do \
	echo -e "$${array1[$$i-1]}_ADDRESS := $${array2[$$i-1]}" >>$@; \
	echo -e "$${array1[$$i-1]}_README := $${array3[$$i-1]}" >>$@; \
	done


include addresses.mk





# links
# query.fasta: $(INPUT_FASTA)
# 	ln -sf $< $@

# query.qual: $(INPUT_QUAL)
# 	ln -sf $< $@

query.fastq: $(INPUT_FASTQ)
	ln -sf $< $@


%.flag:
	$(call download_DB,$($*_ADDRESS),$($*_README),$@)   * the name of the variable containing the address is obtained by % of the target. Variables are conteined in addresses.mk *


define download_DB
	mkdir -p $(basename $3) && \
	cd $(basename $3) && \
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' > DB.info && \
	wget -q -O $(basename $3).gz $1 && \   * cDNA sequence downloaded from ensembl release 66 *
	wget -q $2 && \
	gunzip $(basename $3).gz && \
	makeblastdb -in $(basename $3) -dbtype nucl -title $(basename $3) && \
	cd .. && \
	touch $3
endef


DECONSEQ_DBS := archDB_s1.tar.gz bactDB_s1.tar.gz bactDB_s2.tar.gz bactDB_s3.tar.gz bactDB_s1.tar.gz bactDB_s2.tar.gz senterica_s1.tar.gz virDB_s1.tar.gz
DECONSEQ_FTP := ftp://edwards.sdsu.edu:7009/deconseq/db/ 

deconseq_db.info:
	>$@; \
	$(foreach ADDRESS,$(addprefix $(DECONSEQ_FTP),$(DECONSEQ_DBS)),$(call download_deconseqDB,$(ADDRESS),$@)) \   * for each db file name: join ftp and file name, download the database *
	touch $@

# download a single database
define download_deconseqDB
	date | bawk '!/^[$$,\#+]/ { \
	{ print "database_downloaded:",$$0; } \
	}' >> $@; \
	echo -e "$1" >>$@; \
	if [ ! -s $(basename $(basename $(notdir $1))).pac ]; then \
	wget -q -O $(notdir $1) $1; \
	tar -xvzf $(notdir $1); \
	fi;
endef




# prepares database sequences for indexing
%.db.cleaned.fasta: %.flag
	cat ./$(basename $<)/$(basename $<) | perl -p -e 's/N\n/N/' | perl -p -e 's/^N+//;s/N+$$//;s/N{200,}/\n>split\n/' \
	| prinseq-lite -log -verbose -fasta stdin -min_len 200 -ns_max_p 10 -derep 12345 -out_good stdout -rm_header -seq_id $* -out_bad null >$@


# indexes database sequences for bwa
%.db.cleaned.bwa.pac: %.db.cleaned.fasta
	ALGO=""; \
	SIZE=$(du -sb $< | awk '{ print $$1 }'); \
	if ((SIZE < 4000000000)); then \   * choose the algorithm for indexing according to input file size; limit is 4GB *
	ALGO="is"; \
	else \
	ALGO="bwtsw"; \
	fi; \
	bwa64 index -p $(basename $@) -a $$ALGO $< >$(basename $@).log 2>&1   * use bwa64 from deconseq package *


# remember to edit the database names into DeconSeqConfig.pm, according to names passed to bwa64 index with the -p parameter
DeconSeqConfig.pm:
	ln -sf ${PRJ_ROOT}/local/bin/$@ .


# perform cleaning with deconseq
query.deconseq_cont.fq: query.fastq DeconSeqConfig.pm $(addsuffix .flag, $(SPECIES)) deconseq_db.info
	!threads
	deconseq -f $< \
	-dbs $(DB_FILTER) \
	-dbs_retain $(DB_RETAIN) \
	-i $(ALN_IDENTITY) \
	-c $(shell bawk 'BEGIN {printf "%.0f" ,$(QUERY_COV) * 100}') \
	-t $$THREADNUM \
	-group 1 \
	-id $(call first,$(call split ,_,$@)) \
	-keep_tmp_files



query.deconseq_clean.fq: query.deconseq_cont.fq
	touch $@


# straindata for mira
query.deconseq_clean.strain: query.deconseq_clean.fq
	fastq2tab <$< \
	| bawk ' !/^[$$,\#+]/ { \
	split($$1,a,"_"); \
	if (length(a) > 2) {printf "%s\t%s_%s\n", $$1, a[1], a[2];} \
	else {printf "%s\t%s\n", $$1, a[1];} \
	}' >$@





.PHONY: test
test:
	@echo $(call first,$(call split ,_,query.deconseq_cont.fa))



ALL   += $(addsuffix .flag, $(SPECIES)) \
	 $(addsuffix .db.cleaned.fasta, $(SPECIES)) \
	 DeconSeqConfig.pm \
	 $(addsuffix .db.cleaned.bwa.pac, $(SPECIES)) \
	 query.deconseq_cont.fq \
	 query.deconseq_clean.fq

INTERMEDIATE += 

CLEAN += DeconSeqConfig.pm \
	 query.deconseq_cont.fq \
	 query.deconseq_clean.fq



################################################
### phase_1.mk ends here
