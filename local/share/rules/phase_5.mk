### phase_4.mk --- 
## 
## Filename: phase_4.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Fri Apr 19 12:23:51 2013 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

context prj/454_preprocessing

ADAPTOR_DB ?=
MINI_ADAPTOR_DB ?=


extern ../phase_4/cleaned.fasta as CLEANED_FASTA
extern ../phase_4/cleaned.qual as CLEANED_QUAL

%.fasta: $(CLEANED_FASTA)
	ln -sf $< $@

%.fasta.qual: $(CLEANED_QUAL)
	ln -sf $< $@

# transform into fastq
%.fastq: %.fasta %.fasta.qual
	fastaqual2fastq --fasta $< --qual $^2 > $@


# get report statistics from fastqc
%_fastqc.zip: %.fastq
	!threads
	fastqc --format fastq --noextract --threads $$THREADNUM $<


# prepare preprocessest configuration file
# $(REPEAT_LIB)
process_454.conf:
	echo "adaptor_db=\"$(ADAPTOR_DB)\"" > $@; \
	echo "mini_adaptor_db=\"$(MINI_ADAPTOR_DB)\"" >> $@

# clean sequences using est_process module into est2assembly package
# clean sequences using est_process module into est2assembly package
.NOTPARALLEL: $(IDX1)_cl.log
%_cl.log: %.fasta %.fasta.qual process_454.conf
	!threads
	DIR_NAME="$(basename $@)"; \
	mkdir -p $$DIR_NAME && cd $$DIR_NAME && \   * make directories  *
	preprocessest --dir ../ --technology 454 --project $* --nosff --nomito --noecoli --nordna --norepeats --thread $$THREADNUM --backuptmp --deletetmp --archivelog --config ../$^3 --strain $* ../$< >../$@ 2>&1 && \
	cd .. \
	touch $@   * all done flag *


# get statistic about GC and total length
%_454_cleaned_newbler.info: %_cl.log
	count_fasta -i 50 $(basename $<)/$*_454.cleaned.fasta.x.in_newbler > $@ && \
	fastalen $(basename $<)/$*_454.cleaned.fasta.x.in_newbler | \
	stat_base --mean 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_length",$$0; } \
	}' >> $@ && \
	fastalen $(basename $<)/$*_454.cleaned.fasta.x.in_newbler | \
	stat_base --stdev 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "standard_deviation_length",$$0; } \
	}' >> $@ && \
	fastalen $(basename $<)/$*_454.cleaned.fasta.x.in_newbler | \
	stat_base --median 2 | \
	bawk '!/^[$$,\#+]/ { \
	{ print "median_length",$$0; } \
	}' >> $@ && \
	qual_mean --total < $(basename $<)/$*_454.cleaned.fasta.x.in_newbler.qual | \
	bawk '!/^[$$,\#+]/ { \
	{ print "mean_quality",$$0; } \
	}' >> $@



## mira
%_454_cleaned.fasta: %_cl.log
	ln -sf $(basename $<)/$*_454.cleaned.fasta.x $@

%_454_cleaned.fasta.qual: %_cl.log
	ln -sf $(basename $<)/$*_454.cleaned.fasta.x.qual $@

%_454_cleaned.strain: %_cl.log
	ln -sf $(basename $<)/$*_454.cleaned.fasta.strain $@

%_454_cleaned.xml: %_cl.log
	ln -sf $(basename $<)/$*_454.cleaned.fasta.x.xml $@


## mira unmasked
%_454_cleaned.unmasked.fasta: %_cl.log 
	tar -xjvf $(basename $<)/$*_454.preprocessest.tar.bz2 $*_454.cleaned.fasta.unmasked; \
	mv $*_454.cleaned.fasta.unmasked; $@


## newbler trimmed
%_454_cleaned_newbler.fasta: %_cl.log
	ln -sf $(basename $<)/$*_454.cleaned.fasta.x.in_newbler $@

%_454_cleaned_newbler.fasta.qual: %_cl.log
	ln -sf $(basename $<)/$*_454.cleaned.fasta.x.in_newbler.qual $@



# transform into fastq using sequence files for newbler. Their sequences are really cut
%_454_cleaned_newbler.fastq: %_cl.log %_454_cleaned_newbler.fasta %_454_cleaned_newbler.fasta.qual
	fastaqual2fastq --fasta $< --qual $^2 >$@

# get report statistics from fastqc
%_454_cleaned_newbler_fastqc.zip: %_454_cleaned_newbler.fastq
	!threads
	fastqc --format fastq --noextract --threads $$THREADNUM $<






.PHONY: test
test:
	@echo $(SFF_$(L1DX))


ALL   += $(IDX1).fasta \
	 $(IDX1).fasta.qual \
	 $(IDX1).fastq \
	 $(IDX1)_fastqc.zip \
	 process_454.conf \
	 $(IDX1)_cl.log \
	 $(IDX1)_454_cleaned_newbler.info \
	 $(IDX1)_454_cleaned.fasta \
	 $(IDX1)_454_cleaned.fasta.qual \
	 $(IDX1)_454_cleaned.strain \
	 $(IDX1)_454_cleaned.xml \
	 $(IDX1)_454_cleaned_newbler.fasta \
	 $(IDX1)_454_cleaned_newbler.fasta.qual \
	 $(IDX1)_454_cleaned_newbler.fastq \
	 $(IDX1)_454_cleaned_newbler_fastqc.zip \
	 #$(IDX1)_454_cleaned.unmasked.fasta \


INTERMEDIATE += 


CLEAN += 


######################################################################
### phase_4.mk ends here
