### phase_3.mk --- 
## 
## Filename: phase_3.mk
## Description: 
## Author: Michele
## Maintainer: 
## Created: Fri Apr 12 14:36:10 2013 (+0200)
## Version: 
## Last-Updated: 
##           By: 
##     Update #: 0
## URL: 
## Keywords: 
## Compatibility: 
## 
######################################################################
## 
### Commentary: 
## 
## 
## 
######################################################################
## 
### Change Log:
## 
## 
######################################################################
## 
## This program is free software; you can redistribute it and/or
## modify it under the terms of the GNU General Public License as
## published by the Free Software Foundation; either version 3, or
## (at your option) any later version.
## 
## This program is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
## General Public License for more details.
## 
## You should have received a copy of the GNU General Public License
## along with this program; see the file COPYING.  If not, write to
## the Free Software Foundation, Inc., 51 Franklin Street, Fifth
## Floor, Boston, MA 02110-1301, USA.
## 
######################################################################
## 
### Code:

extern ../phase_1/$(IDX1).fasta as QUERY
extern ../phase_1/$(IDX1).fasta.qual as QUERY_QUAL

extern ../phase_3/STuberosum.tab as ST_CDNA
extern ../phase_3/STuberosumGenome.tab as ST_GENOME
extern ../phase_3/STuberosumGenomeUM.tab as ST_GENOME_UM

extern ../phase_3/TCastaneum.tab as TC_CDNA
extern ../phase_3/TCastaneumGenome.tab as TC_GENOME
extern ../phase_3/TCastaneumGenomeUM.tab as TC_GENOME_UM


query.fasta: $(QUERY)
	ln -sf $< $@

query.qual: $(QUERY_QUAL)
	ln -sf $< $@

STuberosum.tab:
	ln -sf $(ST_CDNA) $@
STuberosumGenome.tab:
	ln -sf $(ST_GENOME) $@
STuberosumGenomeUM.tab:
	ln -sf $(ST_GENOME_UM) $@

TCastaneum.tab:
	ln -sf $(TC_CDNA) $@
TCastaneumGenome.tab:
	ln -sf $(TC_GENOME) $@
TCastaneumGenomeUM.tab:
	ln -sf $(TC_GENOME_UM) $@



%.union: %.tab %GenomeUM.tab
	comm <(cut -f 6 <$< | bsort | uniq) <(cut -f 6 <$^2 | bsort | uniq) \
	| sed 's/[[:space:]]//g' >$@


%.union.fasta: query.fasta %.union
	get_fasta -f $^2 <$< >$@


common_STuberosum-TCastaneum.tab: STuberosum.union TCastaneum.union
	comm -1 -2 <(cut -f 6 <$< | bsort | uniq) <(cut -f 6 <$^2 | bsort | uniq) \
	| sed 's/[[:space:]]//g' >$@

# TCastaneum.union.xml.gz was manually obtained by blastx of TCastaneum.union.fasta
# against NCBI nr via hpc cluster
TCastaneum.union.xml.gz:
	touch $@



TCastaneum.union.tab: TCastaneum.union.xml.gz
	zcat -dc $< \
	| blast_fields -f \
	-r query,query_length \
	-a hit_def \
	-p expect \
	>$@

common_STuberosum-TCastaneum.blast: common_STuberosum-TCastaneum.tab TCastaneum.union.tab
	translate -a -k $^2 1 <$< >$@


# remove = unique to file 1
# TMP=`mktemp -t TMPXXXX` || exit 1;
remove.tab: STuberosum.union TCastaneum.union
	comm -2 -3 <(cut -f 6 <$< | bsort | uniq) <(cut -f 6 <$^2 | bsort | uniq) \
	| sed 's/[[:space:]]//g' >$@



# deconseq_contaminant.fasta was obtained using the web server at:
# http://deconseq.sourceforge.net/.
# the decontamination databases that were selected was:
# Bacterial genomes [2,206 unique genomes, 02/12/11]
# Archaeal genomes [155 unique genomes, 02/12/11]
# Salmonella enterica genomes [52 strains, 12/16/10]
# Bacterial genomes HMP [76,337 WGS sequences, 02/12/11]
# Viral genomes in RefSeq 45 [3,761 unique sequences, 02/12/11]
deconseq_contaminant.fasta:
	touch $@


remove.bacteria.tab: deconseq_contaminant.fasta
	fasta2tab <$< | cut -f 1 >$@

cleaned.fasta: query.fasta remove.tab remove.bacteria.tab
	fasta2tab <$< \
	| filter_1col -v 1 <(cat $^2 $^3 | bsort | uniq) \
	| bsort --key=1,1 \
	| tab2fasta 2 >$@

cleaned.qual: query.qual remove.tab remove.bacteria.tab
	qual2tab <$< \
	| filter_1col -v 1 <(cat $^2 $^3 | bsort | uniq) \
	| bsort --key=1,1 \
	| bawk '!/^[$$,\#+]/ {print ">"$$1"\n"$$2}' >$@



SPECIES_1 := TCastaneum STuberosum
ALL +=  query.fasta \
	query.qual \
	$(addsuffix .tab, $(SPECIES)) \
	$(addsuffix .union, $(SPECIES_1)) \
	$(addsuffix .union.fasta, $(SPECIES_1)) \
	common_STuberosum-TCastaneum.tab \
	TCastaneum.union.tab \
	common_STuberosum-TCastaneum.blast \
	TCastaneum.union.xml.gz \
	deconseq_contaminant.fasta \
	remove.tab \
	remove.bacteria.tab \
	cleaned.fasta \
	cleaned.qual


INTERMEDIATE +=

CLEAN +=


######################################################################
### phase_3.mk ends here
