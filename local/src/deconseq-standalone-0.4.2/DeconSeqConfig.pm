package DeconSeqConfig;

use strict;

use constant DEBUG => 0;
use constant PRINT_STUFF => 1;
use constant VERSION => '0.4.2';
use constant VERSION_INFO => 'DeconSeq version '.VERSION;

use constant ALPHABET => 'ACGTN';

use constant DB_DIR => $ENV{'PWD'}.'/';
use constant TMP_DIR => $ENV{'PWD'}.'/';
use constant OUTPUT_DIR => $ENV{'PWD'}.'/';

use constant PROG_NAME => 'bwa64';  # should be either bwa64 or bwaMAC (based on your system architecture)
use constant PROG_DIR => $ENV{'PRJ_ROOT'}.'/local/bin/';      # should be the location of the PROG_NAME file (use './' if in the same location at the perl script)

use constant DBS => {
    bacts1 => {
	name => 'bacteria_genome_1',
	db => 'bactDB_s1'},
    bacts2 => {
	name => 'bacteria_genome_2',
	db => 'bactDB_s2'},
    bacts3 => {
	name => 'bacteria_genome_3',
	db => 'bactDB_s3'},
    bacthmps1 => {
	name => 'bacteria_hmps_1',
	db => 'bacthmpDB_s1'},
    bacthmps2 => {
	name => 'bacteria_hmps_2',
	db => 'bacthmpDB_s2'},
    archea => {
	name => 'archea_genome',
	db => 'archDB_s1'},
    senterica => {
	name => 'salmonella_enterica_genomes',
	db => 'senterica_s1'},
    vir => {
	name => 'virus_genomes',
	db => 'virDB_s1'},
########################################
    STuberosum => {
	name => 'STuberosum',  #database name used for display and used as input for -dbs and -dbs_retain
	db => 'STuberosum.db.cleaned.bwa'},            #database name as defined with -p for bwa index -p ...
    STuberosumGenomeUM => {
	name => 'STuberosumGenomeUM',
	db => 'STuberosumGenomeUM.db.cleaned.bwa'},
    TCastaneum => {
	name => 'TCastaneum',
	db => 'TCastaneum.db.cleaned.bwa'},
    TCastaneumGenomeUM => {
	name => 'TCastaneumGenomeUM',
	db => 'TCastaneumGenomeUM.db.cleaned.bwa'},
########################################
    danio => {
	name => 'danio',
	db => 'danio.db.cleaned.bwa'},
    danioGenomeUM => {
	name => 'danioGenomeUM',
	db => 'danioGenomeUM.db.cleaned.bwa'}
};
use constant DB_DEFAULT => 'STuberosum';

#######################################################################

use base qw(Exporter);

use vars qw(@EXPORT);

@EXPORT = qw(
             DEBUG
             PRINT_STUFF
             VERSION
             VERSION_INFO
             ALPHABET
             PROG_NAME
             PROG_DIR
             DB_DIR
             TMP_DIR
             OUTPUT_DIR
             DBS
             DB_DEFAULT
             );

1;
